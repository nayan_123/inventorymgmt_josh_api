<?php

namespace App\Classes;


use App\Models\Mwsdeveloperaccount;
use App\Models\Usermarketplace;

class helper
{
    /**
     * Show the application splash screen.
     *
     * @return Response
     */

    protected $marketplace;

    public function highresolutionsimages($asin)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.amazon.com/dp/" . $asin,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $prodImages = [];
        if (strpos($response, "'colorImages':")) {
            $response = explode("'colorImages':", $response);
            $responseresult = explode("'colorToAsin':", $response[1]);
            $responseresult = substr(trim($responseresult[0]), 0, -1);
            $responseresult = json_decode(str_replace("'", '"', $responseresult));
            foreach ($responseresult->initial as $imagesArray) {
                $prodImages[] = $imagesArray->hiRes;
            }
        } elseif ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo 'No high res images found.';
        }
        return $prodImages;
    }

    public function getProductImage($asin_no, Usermarketplace $usermarketplace)
    {

        $this->marketplace = $usermarketplace;

        $services = $this->getReportsClient();
        $asinRequest = new \MarketplaceWebServiceProducts_Model_ASINListType();
        $asinRequest->setASIN([$asin_no]);
        $devaccount = Mwsdeveloperaccount::where('marketplace', $usermarketplace->country)->first();
        $request = new \MarketplaceWebServiceProducts_Model_GetMatchingProductRequest();
        $request->setMarketplaceId($devaccount->marketplace_id);
        $request->setASINList($asinRequest);
        $request->setSellerId(base64_decode($usermarketplace->mws_sellerid));
        $request->setMWSAuthToken(base64_decode($usermarketplace->mws_authtoken));
        return $response = $this->invokeListProduct($services, $request);
    }


    protected function getReportsClient()
    {
        $uri = '/Products/2011-10-01';
        list($access_key, $secret_key, $config) = $this->getKeys($uri);
        return new \MarketplaceWebServiceProducts_Client(
            $access_key,
            $secret_key,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION'),
            $config
        );
    }

    private function getKeys($uri = '')
    {
        add_to_path('Libraries');
        $dev_account = Mwsdeveloperaccount::where('marketplace', $this->marketplace->country)->first();

        return [
            base64_decode($dev_account->access_key),
            base64_decode($dev_account->secret_key),
            self::getMWSConfig($dev_account, $uri)
        ];
    }

    public static function getMWSConfig(Mwsdeveloperaccount $devaccount, $uri){
        return [
            'ServiceURL' => "{$devaccount->marketplace_url}{$uri}",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 10,
        ];
    }

    public function invokeListProduct(\MarketplaceWebServiceProducts_Interface $service, $request)
    {
        try {
            $response = $service->getMatchingProduct($request);
            print_r($response);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $imageURL = $title = '';
            foreach ($dom->getElementsByTagName('URL') as $creator) {
                $imageURL = $creator->nodeValue;
            }
            foreach ($dom->getElementsByTagName('Title') as $creator) {
                $title = $creator->nodeValue;
            }
            $data['imageURL'] = $imageURL;
            $data['title'] = $title;
            return $data;

        } catch (\MarketplaceWebServiceProducts_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }


}
