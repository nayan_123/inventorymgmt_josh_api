<?php

namespace App\Models;

use App\Classes\helper;
use App\Jobs\CalculateSalesDaily;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\Mws_import_order;

class Mws_listing_defect_data extends Model
{
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'sku',
        'product_name',
        'asin',
        'field_name',
        'alert_type',
        'current_value',
        'last_updated',
        'alert_name',
        'status',
        'explanation'
    ];
    //
    protected $table = 'mws_product_listing_quality';

    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }

    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }

    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Product Listing Quality report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->prod_name = utf8_encode($row['product-name']);
                    $productId->save();
                }
                if (isset($row['sku'])) {
                    $mwsListingDefectData = Mws_listing_defect_data::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->first();
                    if ($mwsListingDefectData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsListingDefectData = new Mws_listing_defect_data();
                    }
                    $mwsListingDefectData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsListingDefectData->product_id = $productId->id;
                    $mwsListingDefectData->sku = $row['sku'];
                    $mwsListingDefectData->product_name = utf8_encode($row['product-name']);;
                    $mwsListingDefectData->asin = $row['asin'];
                    $mwsListingDefectData->field_name = $row['field-name'];
                    $mwsListingDefectData->alert_type = $row['alert-type'];
                    $mwsListingDefectData->current_value = $row['current-value'];
                    $mwsListingDefectData->last_updated = $row['last-updated'];
                    $mwsListingDefectData->alert_name = $row['alert-name'];
                    $mwsListingDefectData->status = $row['status'];
                    $mwsListingDefectData->explanation = $row['explanation'];
                    $mwsListingDefectData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Product Listing Quality report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();

    }

}
