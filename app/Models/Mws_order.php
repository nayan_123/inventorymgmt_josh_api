<?php

namespace App\Models;

use App\Classes\helper;
use App\Jobs\CalculateSalesDaily;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\Mws_import_order;

class Mws_order extends Model
{
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'amazon_order_id',
        'merchant_order_id',
        'shipment_id',
        'shipment_item_id',
        'amazon_order_item_id',
        'merchant_order_item_id',
        'purchase_date',
        'payments_date',
        'shipment_date',
        'reporting_date',
        'buyer_email',
        'buyer_name',
        'buyer_phone_number',
        'sku',
        'product_name',
        'quantity_shipped',
        'currency',
        'item_tax',
        'item_price',
        'shipping_price',
        'shipping_tax',
        'gift_wrap_price',
        'gift_wrap_tax',
        'ship_service_level',
        'recipient_name',
        'ship_address_1',
        'ship_address_2',
        'ship_address_3',
        'ship_city',
        'ship_state',
        'ship_postal_code',
        'ship_country',
        'ship_phone_number',
        'bill_address_1',
        'bill_address_2',
        'bill_address_3',
        'bill_city',
        'bill_state',
        'bill_postal_code',
        'bill_country',
        'item_promotion_discount',
        'ship_promotion_discount',
        'carrier',
        'tracking_number',
        'estimated_arrival_date',
        'fulfillment_center_id',
        'fulfillment_channel',
        'sales_channel'
    ];
    //
    protected $table = 'mws_orders';

    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }

    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }

    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Order report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->prod_name = utf8_encode($row['product-name']);
                    $productId->fulfilledby = $row['fulfillment-channel'];
                    $productId->your_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                    $productId->sales_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                    $productId->currency = $row['currency'];
                    $productId->save();
                }
                if (isset($row['amazon-order-id'])) {
                    $mwsOrder = Mws_order::where('amazon_order_id', $row['amazon-order-id'])->where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->first();
                    if ($mwsOrder) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsOrder = new Mws_order();
                    }
                    $mwsOrder->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsOrder->product_id = $productId->id;
                    $mwsOrder->amazon_order_id = $row['amazon-order-id'];
                    $mwsOrder->merchant_order_id = $row['merchant-order-id'];
                    $mwsOrder->shipment_id = $row['shipment-id'];
                    $mwsOrder->shipment_item_id = $row['shipment-item-id'];
                    $mwsOrder->amazon_order_item_id = $row['amazon-order-item-id'];
                    $mwsOrder->merchant_order_item_id = $row['merchant-order-item-id'];
                    $mwsOrder->purchase_date = $row['purchase-date'];
                    $mwsOrder->payments_date = $row['payments-date'];
                    $mwsOrder->shipment_date = $row['shipment-date'];
                    $mwsOrder->reporting_date = $row['reporting-date'];
                    $mwsOrder->buyer_email = $row['buyer-email'];
                    $mwsOrder->buyer_name = $row['buyer-name'];
                    $mwsOrder->buyer_phone_number = $row['buyer-phone-number'];
                    $mwsOrder->sku = $row['sku'];
                    $mwsOrder->product_name = utf8_encode($row['product-name']);
                    $mwsOrder->quantity_shipped = $row['quantity-shipped'];
                    $mwsOrder->currency = $row['currency'];
                    $mwsOrder->item_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                    $mwsOrder->item_tax = (!empty($row['item-tax']) ? $row['item-tax'] : '0');
                    $mwsOrder->shipping_price = (!empty($row['shipping-price']) ? $row['shipping-price'] : '0');
                    $mwsOrder->shipping_tax = (!empty($row['shipping-tax']) ? $row['shipping-tax'] : '0');
                    $mwsOrder->gift_wrap_price = (!empty($row['gift-wrap-price']) ? $row['gift-wrap-price'] : '0');
                    $mwsOrder->gift_wrap_tax = (!empty($row['gift-wrap-tax']) ? $row['gift-wrap-tax'] : '0');
                    $mwsOrder->ship_service_level = $row['ship-service-level'];
                    $mwsOrder->recipient_name = utf8_encode($row['recipient-name']);
                    $mwsOrder->ship_address_1 = utf8_encode($row['ship-address-1']);
                    $mwsOrder->ship_address_2 = utf8_encode($row['ship-address-2']);
                    $mwsOrder->ship_address_3 = utf8_encode($row['ship-address-3']);
                    $mwsOrder->ship_city = utf8_encode($row['ship-city']);
                    $mwsOrder->ship_state = utf8_encode($row['ship-state']);
                    $mwsOrder->ship_postal_code = utf8_encode($row['ship-postal-code']);
                    $mwsOrder->ship_country = utf8_encode($row['ship-country']);
                    $mwsOrder->ship_phone_number = $row['ship-phone-number'];
                    $mwsOrder->bill_address_1 = utf8_encode($row['bill-address-1']);
                    $mwsOrder->bill_address_2 = utf8_encode($row['bill-address-2']);
                    $mwsOrder->bill_address_3 = utf8_encode($row['bill-address-3']);
                    $mwsOrder->bill_city = utf8_encode($row['bill-city']);
                    $mwsOrder->bill_state = utf8_encode($row['bill-state']);
                    $mwsOrder->bill_postal_code = utf8_encode($row['bill-postal-code']);
                    $mwsOrder->bill_country = utf8_encode($row['bill-country']);
                    $mwsOrder->item_promotion_discount = (!empty($row['item-promotion-discount'] ? $row['item-promotion-discount'] : 0));
                    $mwsOrder->ship_promotion_discount = (!empty($row['ship-promotion-discount'] ? $row['ship-promotion-discount'] : 0));
                    $mwsOrder->carrier = utf8_encode($row['carrier']);
                    $mwsOrder->tracking_number = $row['tracking-number'];
                    $mwsOrder->estimated_arrival_date = $row['estimated-arrival-date'];
                    $mwsOrder->fulfillment_center_id = $row['fulfillment-center-id'];
                    $mwsOrder->fulfillment_channel = $row['fulfillment-channel'];
                    $mwsOrder->sales_channel = $row['sales-channel'];
                    $mwsOrder->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Order report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();

        if ($amazon_report->amz_dateitetration_id > 0) {
            $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
            if (!empty($amzDateitetration->id)) {
                print_r($amzDateitetration);
                $amzDateitetration->OrderStatus = '1';
                $amzDateitetration->save();
            }
        }
    }

}
