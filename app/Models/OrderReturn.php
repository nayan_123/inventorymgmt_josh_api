<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderReturn extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "mws_order_returns";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_marketplace_id', 'product_id', 'order_id', 'return_date', 'sku', 'asin', 'fnsku', 'product_name', 'quantity',
        'fulfillment_center_id', 'detailed_disposition', 'reason', 'status', 'license_plate_number', 'customer_comments',
    ];

    /**
     * Get the order that owns the phone.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'amazon_order_id');
    }
}
