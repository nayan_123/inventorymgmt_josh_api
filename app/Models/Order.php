<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "mws_orders";

    /**
     * Get the order return record associated with the order.
     */
    public function orderReturn()
    {
        return $this->hasOne('App\Models\OrderReturn');
    }

}
