<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_product extends Model
{
    //
    protected $table = 'mws_products';
    protected $fillable = [
        'user_marketplace_id',
        'sku',
        'fnsku',
        'asin',
        'upc',
        'prod_name',
        'prod_image',
        'prod_group',
        'brand',
        'fulfilledby',
        'haslocalinventory',
        'your_price',
        'sales_price',
        'longestside',
        'medianside',
        'shortestside',
        'lengthandgridth',
        'unitofdimensions',
        'itempackweight',
        'unitofweight',
        'productsizeweightband',
        'currency',
    ];

    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    public function mws_afn_inventory(){
        return $this->hasOne('App\Models\Mws_afn_inventory_data','product_id');
    }

    public function vendor_product()
    {
        return $this->hasMany('App\Models\Vendor_product', 'product_id');
    }

    public function supplier_product()
    {
        return $this->hasMany('App\Models\Supplier_product', 'product_id');
    }

    public function calculated_sales_qty()
    {
        return $this->hasMany('App\Models\Calculated_sales_qty', 'product_id');
    }

    public function daily_logic_calculations()
    {
        return $this->hasMany('App\Models\Daily_logic_Calculations', 'prod_id');
    }
    public function inbound_shipment_items()
    {
        return $this->hasMany('App\Models\Inbound_shipment_items', 'product_id');
    }
    public function mws_excess_inventory_data()
    {
        return $this->hasMany('App\Models\Mws_excess_inventory_data', 'product_id');
    }
    public function mws_adjustment_inventory_datas()
    {
        return $this->hasMany('App\Models\Mws_adjustment_inventory_data', 'product_id');
    }
    public function mws_reserved_inventory()
    {
        return $this->hasOne('App\Models\Mws_reserved_inventory', 'product_id');
    }
    public function mws_inventory_details()
    {
        return $this->hasMany('App\Models\Mws_inventory_details', 'product_id');
    }
    public function mws_orders()
    {
        return $this->hasMany('App\Models\Mws_order', 'product_id');
    }
    public function mws_order_returns()
    {
        return $this->hasMany('App\Models\Mws_order_returns', 'product_id')->orderBy('return_date','DESC');
    }
    public function mws_out_of_stock_detail()
    {
        return $this->hasMany('App\Models\Mws_out_of_stock_detail', 'product_id');
    }
    public function mws_calculated_inventory_daily()
    {
        return $this->hasMany('App\Models\Mws_calculated_inventory_daily', 'product_id');
    }
    public function mws_warehouse_wise_inventory()
    {
        return $this->hasMany('App\Models\Mws_warehouse_wise_inventory', 'product_id');
    }

    public function warehouse_product()
    {
        return $this->hasMany('App\Models\Warehouse_product', 'product_id');
    }

    public function sales_total_trend_rate(){
        return $this->hasMany('App\Models\Sales_total_trend_rate','user_marketplace_id');
    }

    public function purchase_order_details(){
        $this->belongsTo('App\Models\Purchase_order_details','product_id');
    }
    public function mws_unsuppressed_inventory_data(){
        return $this->hasOne('App\Models\Mws_unsuppressed_inventory_data','product_id');
    }
    public function product_assign_supply_logic()
    {
        return $this->hasMany('App\Models\Product_assign_supply_logic', 'user_marketplace_id');
    }
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Product report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {
            Log::debug("Reading a row from report {$amazon_report->id}");

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            $mailForward = 0;
            $storageStatus = 'status unknown';
            $mwsProduct = '';
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }

                $row = array_combine($headers, $row);
                $prodName = utf8_encode($row['product-name']);
                $mwsProduct = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if ($mwsProduct) {
                    $storageStatus = 'updated';
                    $fetchImage = new helper();
                    $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                    if($mwsProduct->prod_name=='' || $mwsProduct->prod_name=='null' || $mwsProduct->prod_name==NULL){
                        $prodName = utf8_encode($productData['title']);
                        Log::debug("MWS Products $prodName: {$mwsProduct->id}");
                    }
                } else {
                    $storageStatus = 'created';
                    $fetchImage = new helper();
                    $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $mwsProduct = new Mws_product();
                    $mwsProduct->prod_image = ($imageURL ? $imageURL[0] : '');
                    if($mwsProduct->prod_name=='' || $mwsProduct->prod_name=='null' || $mwsProduct->prod_name==NULL){
                        $prodName = utf8_encode($productData['title']);
                        Log::debug("MWS Products $prodName: ");
                    }
                }
                $mwsProduct->user_marketplace_id = $amazon_report->user_marketplace_id;
                $mwsProduct->sku = $row['sku'];
                $mwsProduct->fnsku = $row['fnsku'];
                $mwsProduct->asin = $row['asin'];
                $mwsProduct->prod_name = $prodName;
                $mwsProduct->prod_group = $row['product-group'];
                $mwsProduct->brand = $row['brand'];
                $mwsProduct->fulfilledby = $row['fulfilled-by'];
                $mwsProduct->haslocalinventory = (isset($row['has-local-inventory']) ? $row['has-local-inventory'] : '');
                $mwsProduct->your_price = $row['your-price'];
                $mwsProduct->sales_price = $row['sales-price'];
                $mwsProduct->longestside = $row['longest-side'];
                $mwsProduct->medianside = $row['median-side'];
                $mwsProduct->shortestside = $row['shortest-side'];
                $mwsProduct->lengthandgridth = $row['length-and-girth'];
                $mwsProduct->unitofdimensions = $row['unit-of-dimension'];
                $mwsProduct->itempackweight = $row['item-package-weight'];
                $mwsProduct->unitofweight = $row['unit-of-weight'];
                $mwsProduct->productsizeweightband = (isset($row['product-size-weight-band']) ? $row['product-size-weight-band'] : '');
                $mwsProduct->save();
            }
        }
        Log::debug("Ended Reading MWS Product report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}
