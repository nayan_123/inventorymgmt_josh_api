<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amzdateiteration extends Model
{
    //
    protected $table = 'amzdateiteration';

    protected $fillable = [
        'user_marketplace_id',
        'startDate',
        'endDate',
        'OrderStatus',
        'InventoryStatus',
        'ReimburseStatus',
        'ReturnStatus',
        'FinanceStatus',
        'FeedbackStatus',
        'CustomersalesStatus',
        'InventoryshipmentStatus',
        'ShipmentOrderRemovalStatus'
    ];

    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}
