<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Amazon_requestlog extends Model
{
    //
    protected $table = 'amazon_reportrequestlog';
    public static $MERCHANT_LISTINGS = "_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_";
    public static $ORDER_MERCHANGT_LISTINGS = "_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_";
    public static $MWS_ORDER_RETURNS_LISTINGS="_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_";
    public static $MWS_MERCHANT_LISTING_DEFECT_DATA="_GET_MERCHANT_LISTINGS_DEFECT_DATA_";
    public static $DONE = "_DONE_";
    public static $SUBMITTED = "_SUBMITTED_";
    public static $IN_PROGRESS = "_IN_PROGRESS_";
    public static $CANCELLED = "_CANCELLED_";
    public static $NO_DATA = "_DONE_NO_DATA_";

    protected $fillable = [
        'user_marketplace_id', 'marketplace_id', 'request_id', 'report_id', 'status', 'available_at',
        'last_checked', 'report_type', 'request_response','processed', 'amz_dateitetration_id'
    ];

    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    public static function getPendingReports($usermarketplaceid){
        return self::where('last_checked', "<" , Carbon::parse('1 minutes ago'))
            ->whereNotIn("report_type",["_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_"])
            ->whereUserMarketplaceId($usermarketplaceid)
            ->whereProcessed(0)->get();
    }

    public static function getPendingReportsHigh($usermarketplaceid){
        return self::where('last_checked', "<" , Carbon::parse('1 minutes ago'))
            ->whereUserMarketplaceId($usermarketplaceid)
            ->whereReportType('_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_')
            ->whereProcessed(0)->get();
    }
}
