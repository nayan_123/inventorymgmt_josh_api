<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mwsdeveloperaccount extends Model
{
    protected $table = 'mws_developer_accounts';

    protected $fillable = [
        'developer_accno',
        'marketplace_id',
        'access_key',
        'secret_key',
        'marketplace_url',
        'marketplace',
        'marketplace_status',
        'marketplace_country'
    ];

    public function mws_market_place(){
        return $this->belongsTo('App\Models\User,marketplace','country','marketplace');
    }
}