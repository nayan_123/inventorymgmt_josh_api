<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMarketplace extends Model
{
    protected $table = 'mws_user_marketplace';

    protected $fillable = [
        'user_id', 'mws_sellerid', 'mws_marketplaceid', 'mws_authtoken', 'country', 'mws_report_url', 'amazonstorename',
        'apiactive', 'created_at', 'updated_at', 'deleted_at', 'api_token',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
