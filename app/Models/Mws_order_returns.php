<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;


class Mws_order_returns extends Model
{
    //
    protected $table="mws_order_returns";
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'order_id',
        'return_date',
        'sku',
        'asin',
        'fnsku',
        'product_name',
        'quantity',
        'fulfillment_center_id',
        'detailed_disposition',
        'reason',
        'status',
        'license_plate_number',
        'customer_comments'
       
    ];

    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }

    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Order Return report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            $storageStatus = 'status unknown';

            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }

                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if(isset($productId->id)) {
                    if (isset($row['order-id'])) {
                        $mwsOrderReturns = Mws_order_returns::where('order_id', $row['order-id'])->where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if ($mwsOrderReturns) {
                            $storageStatus = 'updated';
                        } else {
                            $storageStatus = 'created';
                            $mwsOrderReturns = new Mws_order_returns();
                        }
                        $mwsOrderReturns->user_marketplace_id = $amazon_report->user_marketplace_id;
                        $mwsOrderReturns->product_id = $productId->id;
                        $mwsOrderReturns->order_id = $row['order-id'];
                        $mwsOrderReturns->sku = $row['sku'];
                        $mwsOrderReturns->asin = $row['asin'];
                        $mwsOrderReturns->fnsku = $row['fnsku'];
                        $mwsOrderReturns->product_name = utf8_encode($row['product-name']);
                        $mwsOrderReturns->quantity = $row['quantity'];
                        $mwsOrderReturns->fulfillment_center_id = $row['fulfillment-center-id'];
                        $mwsOrderReturns->detailed_disposition = $row['detailed-disposition'];
                        $mwsOrderReturns->reason = utf8_encode($row['reason']);
                        $mwsOrderReturns->status = $row['status'];
                        $mwsOrderReturns->license_plate_number = $row['license-plate-number'];
                        $mwsOrderReturns->customer_comments = utf8_encode($row['customer-comments']);
                        $mwsOrderReturns->return_date = $row['return-date'];
                        $mwsOrderReturns->save();
                    }
                }
            }
        }
        $amazon_report->processed = 1;
        $amazon_report->save();
        if ($amazon_report->amz_dateitetration_id > 0) {
            $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
            if (!empty($amzDateitetration->id)) {
                print_r($amzDateitetration);
                $amzDateitetration->ReturnStatus = '1';
                $amzDateitetration->save();
            }
        }
        Log::debug("Ended Reading MWS Order Return report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
    }
}
