<?php

use Stringy\Stringy;
use App\Models\Usermarketplace;
use App\Models\Mws_product;

function TimeToServer($DateAndTime)
{
    if ($DateAndTime == '') {
        $datetime = new DateTime();
    } else {
        $datetime = new DateTime($DateAndTime);
    }
    $la_time = new DateTimeZone('America/Los_Angeles');
    $datetime->setTimezone($la_time);
    return $datetime->format('Y-m-d H:i:s');
}

function sign($number)
{
    return ($number > 0) ? 1 : (($number < 0) ? -1 : 0);
}


if(!function_exists('replace_keywords')) {
    function replace_keywords($array, $message)
    {
        foreach ($array as $k => $v) {
            $message = str_replace($k, $v, $message);
        }
        return $message;
    }
}

if(!function_exists('get_user_marketplace_list')){
    function get_user_marketplace_list($user_id){
        return Usermarketplace::where(array('user_id'=>$user_id))->get()->toArray();
    }
}

if(!function_exists('get_usermarketplace')){
    function get_usermarketplace($user_id){
        return Usermarketplace::with('mws_market_place_country')->where(array('user_id'=>$user_id))->get()->toArray();
    }
}

function get_dashed_name($object)
{
    return Stringy::create(get_real_class($object))->dasherize()->__toString();
}
function get_real_class($object)
{
    $class_name = get_class($object);
    if (preg_match('@\\\\([\w]+)$@', $class_name, $matches)) {
        $class_name = $matches[1];
    }
    return $class_name;
}
function add_to_path($path)
{
    $lib_path = app_path($path);
    $include_path = get_include_path();
    if (!str_contains($include_path, $lib_path)) {
        set_include_path($include_path . PATH_SEPARATOR . $lib_path);
    }
}

if(!function_exists('get_parent_usermarketplace')){
    function get_parent_usermarketplace($user_id){
        $total_marketplace = array();
        $get_parent_user = Users::where(array('id'=>$user_id))->first();
        if(!empty($get_parent_user) && $get_parent_user->parent_id != ''){
            $total_marketplace =  Usermarketplace::with('mws_market_place_country')->where(array('user_id'=>$get_parent_user->parent_id))->get()->toArray();
            return $total_marketplace;
        }else{
            return $total_marketplace;
        }
    }
}

if(!function_exists('get_product_details')){
    function get_product_details($product_id){
        $get_product = Mws_product::where(array('id'=>$product_id))->first();
        return $get_product->prod_name;
    }
}
if(!function_exists('get_product_sku')){
    function get_product_sku($product_id){
        $get_product = Mws_product::where(array('id'=>$product_id))->first();
        return $get_product->sku;
    }
}