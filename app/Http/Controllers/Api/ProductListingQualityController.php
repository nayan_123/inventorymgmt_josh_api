<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\ProductListingResource;
use App\Models\Mws_listing_defect_data;
use Illuminate\Http\Request;
use Log;

class ProductListingQualityController extends BaseController
{
    /**
     * get product listing
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            // Get product listing details
            $productListing = Mws_listing_defect_data::where('user_marketplace_id', $request->input('marketplace_id'));

            if ($request->has('sku')) {
                $productListing->where('sku', $request->input('sku'));
            }

            if ($request->has('status')) {
                $productListing->where('status', $request->input('status'));
            }

            $productListing = $productListing->paginate(10);

            if (!$productListing) {
                return $this->sendError(trans('There are no product listing of given marketplace id.'));
            }

            // All good so return the response
            return $this->sendResponse([
                'first_page_url' => $request->fullUrlWithQuery(['page' => '1']),
                'last_page' => $productListing->lastPage(),
                'last_page_url' => $productListing->hasMorePages() ? $request->fullUrlWithQuery(['page' => $productListing->lastPage()]) : '',
                'next_page_url' => $productListing->hasMorePages() ? $request->fullUrlWithQuery(['page' => $productListing->currentPage() + 1]) : '',
                'total_count' => $productListing->total(),
                'current_page' => $productListing->currentPage(),
                'previous_page' => $productListing->onFirstPage() ? false : true,
                'previous_page_url' => $productListing->onFirstPage() ? '' : $request->fullUrlWithQuery(['page' => $productListing->currentPage() - 1]),
                'data' => ProductListingResource::collection($productListing),
            ]);
        } catch (\Exception $e) {
            // Log Error message
            Log::error("App\Http\Controller\Api\ProductListingQualityController::index | Error while fetching product detail: {$e->getMessage()}");
            return $this->sendError(trans('Whoops! Something went wrong. Please try again.'), 500);
        }
    }
}
