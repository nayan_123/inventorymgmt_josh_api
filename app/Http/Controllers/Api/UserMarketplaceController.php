<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\UserMarketplaceResource;
use App\User;
use Illuminate\Http\Request;
use Log;

class UserMarketplaceController extends BaseController
{
    /**
     * get user marketplace
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            // Get user details
            if ($request->input('user_id')) {
                $user = User::where('id', $request->input('user_id'))->get();
            } elseif ($request->input('seller_id')) {
                $sellerId = base64_encode($request->input('seller_id'));
                $user = User::whereHas('userMarketplace', function ($q) use ($sellerId) {
                    $q->where('mws_sellerid', $sellerId);
                })->get();
            } elseif ($request->input('email')) {
                $user = User::where('email', $request->input('email'))->get();
            } else {
                $user = User::whereHas('userMarketplace', function ($q) {
                    $q->whereRaw("1=1");
                })->get();
            }

            if ($user->isEmpty()) {
                return $this->sendError(trans('There are no data of your provided details in our system.'));
            }

            // All good so return the response
            return $this->sendResponse([
                'data' => UserMarketplaceResource::collection($user),
            ]);
        } catch (\Exception $e) {
            // Log Error message
            Log::error("App\Http\Controller\Api\UserMarketplaceController::index | Error while fetching user marketplace: {$e->getMessage()}");
            return $this->sendError(trans('Whoops! Something went wrong. Please try again.'), 500);
        }
    }
}
