<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\OrderReturnResource;
use App\Models\OrderReturn;
use Illuminate\Http\Request;
use Log;

class OrderReturnController extends BaseController
{
    /**
     * get order return
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            // Get order return details
            $orderReturn = OrderReturn::with('order')
                ->where('user_marketplace_id', $request->input('marketplace_id'));

            if ($request->has('return_date')) {
                $orderReturn->where('return_date', $request->input('return_date'));
            }

            if ($request->has('order_id')) {
                $orderReturn->where('order_id', $request->input('order_id'));
            }

            if ($request->has('sku')) {
                $orderReturn->where('sku', $request->input('sku'));
            }

            if ($request->has('reason')) {
                $orderReturn->where('reason', $request->input('reason'));
            }

            $orderReturn = $orderReturn->paginate(10);

            if (!$orderReturn) {
                return $this->sendError(trans('There are no order return of given marketplace id.'));
            }

            // All good so return the response
            return $this->sendResponse([
                'first_page_url' => $request->fullUrlWithQuery(['page' => '1']),
                'last_page' => $orderReturn->lastPage(),
                'last_page_url' => $orderReturn->hasMorePages() ? $request->fullUrlWithQuery(['page' => $orderReturn->lastPage()]) : '',
                'next_page_url' => $orderReturn->hasMorePages() ? $request->fullUrlWithQuery(['page' => $orderReturn->currentPage() + 1]) : '',
                'total_count' => $orderReturn->total(),
                'current_page' => $orderReturn->currentPage(),
                'previous_page' => $orderReturn->onFirstPage() ? false : true,
                'previous_page_url' => $orderReturn->onFirstPage() ? '' : $request->fullUrlWithQuery(['page' => $orderReturn->currentPage() - 1]),
                'data' => OrderReturnResource::collection($orderReturn),
            ]);
        } catch (\Exception $e) {
            // Log Error message
            Log::error("App\Http\Controller\Api\OrderReturnController::index | Error while fetching order return: {$e->getMessage()}");
            return $this->sendError(trans('Whoops! Something went wrong. Please try again.'), 500);
        }
    }
}
