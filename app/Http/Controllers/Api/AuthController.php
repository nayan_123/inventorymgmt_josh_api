<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Api\Login;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Log;

class AuthController extends BaseController
{
    /**
     * login to user
     *
     * @param \App\Http\Requests\Api\Login $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Login $request)
    {
        try {
            // Get user details
            $user = User::getUserByEmail($request->input('email'));
            if (!$user) {
                return $this->sendError(trans('There is no account for your email.'));
            }

            // Authorize user credentials
            $credentials = $request->only(['email', 'password']);

            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->sendError(trans('There was an error with your E-Mail/Password combination. Please try again.'));
            }

            // If user is inactivated
            if ($user->active == 0) {
                return $this->sendError(trans('Your account has been deactivated. Please contact system administrator.'));
            }

            // All good so return the response
            return $this->sendResponse(['data' => [
                'loginToken' => $token,
                'user' => new UserResource($user),
            ]]);
        } catch (\Exception $e) {
            // Log Error message
            Log::error("App\Http\Controller\Api\AuthController::authenticate | Authentication error: {$e->getMessage()}");
            return $this->sendError(trans('Whoops! Something went wrong. Please try again.'), 500);
        }
    }

    /**
     * Logout
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            // Get bearer token
            $bearerToken = explode(' ', $request->header('Authorization'))[1];

            if (JWTAuth::invalidate($bearerToken)) {
                // All good so return the response
                return $this->sendResponse(null, trans('You have successfully logged out!'));
            } else {
                // Return error message when token Unauthorized
                return $this->sendError(trans('Unauthorized token.'));
            }
        } catch (\Exception $e) {
            // Log Error message
            Log::error("App\Http\Controller\Api\AuthController::logout | User logout error: {$e->getMessage()}");
            return $this->sendError(trans('Whoops! Something went wrong. Please try again.'), 500);
        }
    }
}
