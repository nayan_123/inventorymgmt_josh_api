<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if (!$user) {
                return response()->json([
                    'status' => 0,
                    'message' => trans('api-message.FAILED_TO_VALIDATE_TOKEN'),
                ], 401);
            }

            // If user is inactivated
            if ($user->status == config('constant.INACTIVE_FLAG')) {
                return response()->json([
                    'status' => 0,
                    'message' => trans('api-message.USER_ACCOUNT_INACTIVATED_BY_ADMIN'),
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 0,
                'message' => trans('api-message.TOKEN_EXPIRED'),
            ], $e->getStatusCode());
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => trans('api-message.INVALID_OR_BLACKLISTED_TOKEN'),
            ], $e->getStatusCode());
        }

        return $next($request);
    }
}
