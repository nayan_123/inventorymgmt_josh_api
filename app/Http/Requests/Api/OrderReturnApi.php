<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\FormRequest;
use Illuminate\Validation\Rule;

class OrderReturnApi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'marketplace_id' => 'required|integer|min:1',
            'return_date' => ['nullable', 'date'],
            'order_id' => 'nullable',
            'sku' => 'nullable',
            'reason' => 'nullable',
        ];
    }
}
