<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'product_id' => (string) $this->product_id,
            'user_marketplace_id' => (string) $this->user_marketplace_id,
            'sku' => (string) $this->sku,
            'product_name' => (string) $this->product_name,
            'asin' => (string) $this->asin,
            'field_name' => (string) $this->field_name,
            'alert_type' => (string) $this->alert_type,
            'current_value' => (string) $this->current_value,
            'last_updated' => (string) $this->last_updated,
            'alert_name' => (string) $this->alert_name,
            'status' => (string) $this->status,
            'explanation' => (string) $this->explanation,
        ];
    }
}
