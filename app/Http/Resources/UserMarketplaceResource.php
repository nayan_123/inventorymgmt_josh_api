<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserMarketplaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => (string) $this->name,
            'email' => (string) $this->email,
            'marketplace_detail' => UserMarketplaceUserDetailResource::collection($this->userMarketplace),
        ];
    }
}
