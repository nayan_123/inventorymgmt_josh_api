<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderReturnResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'user_marketplace_id' => (string) $this->user_marketplace_id,
            'product_id' => (string) $this->product_id,
            'order_id' => (string) $this->order_id,
            'return_date' => (string) $this->return_date,
            'sku' => (string) $this->sku,
            'asin' => (string) $this->asin,
            'fnsku' => (string) $this->fnsku,
            'product_name' => (string) $this->product_name,
            'quantity' => (string) $this->quantity,
            'fulfillment_center_id' => (string) $this->fulfillment_center_id,
            'detailed_disposition' => (string) $this->detailed_disposition,
            'reason' => (string) $this->reason,
            'status' => (string) $this->status,
            'license_plate_number' => (string) $this->license_plate_number,
            'customer_comments' => (string) $this->customer_comments,
            'email' => (string) $this->order['buyer_email'],
        ];
    }
}
