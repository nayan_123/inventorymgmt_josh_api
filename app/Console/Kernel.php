<?php

namespace App\Console;

use App\Jobs\ProcessReportsHighJob;
use App\Jobs\ProcessReportsJob;
use App\Jobs\RequestReport;
use App\Models\Amazon_requestlog;
use App\Models\Amzdateiteration;
use App\Models\UserMarketplace;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->requestHistoricalOrderReport($schedule);
        $this->requestOrderReport($schedule);
        $this->processReports($schedule);
        $this->processReportsHigh($schedule);
        $this->requestProductReport($schedule);
        $this->requestOrderReturnReport($schedule);
        $this->requestHistoricalOrderReturnReport($schedule);
        $this->requestQualitySuppressedReport($schedule);
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    private function processReports(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\ProcessReportsJob' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $processReportJob = new ProcessReportsJob($marketplace);
                    dispatch($processReportJob)->onQueue('low');
                }
            }
        })->everyFifteenMinutes();
    }

    private function processReportsHigh(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                $loopBreakStatus = 0;
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\ProcessReportsHighJob' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $processReportJob = new ProcessReportsHighJob($marketplace);
                    dispatch($processReportJob)->onQueue('high');
                }
            }
        })->everyFifteenMinutes();
    }

    private function requestOrderReturnReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at','=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('ReturnStatus', '0')->first();
                if (empty($amzDateitetration)) {
                    $orderReturnRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_', $amzDateitetration);
                    dispatch($orderReturnRequestJob)->onQueue('low');
                }
            }
        })->cron("0 7,22 * * *");
    }

    private function requestQualitySuppressedReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $orderReturnRequestJob = new RequestReport($marketplace, '_GET_MERCHANT_LISTINGS_DEFECT_DATA_');
                dispatch($orderReturnRequestJob)->onQueue('low');
            }
        })->hourly();
    }

    private function requestHistoricalOrderReturnReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at','=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('ReturnStatus', '0')->first();
                if (!empty($amzDateitetration)) {
                    $orderReturnRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_', $amzDateitetration);
                    dispatch($orderReturnRequestJob)->onQueue('low');
                }
            }
        })->hourly();
    }

    private function requestOrderReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at','=', date('d'))->where('report_type', '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '0')->first();
                if (empty($amzDateitetration)) {
                    $orderRequestJob = new RequestReport($marketplace, '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_', $amzDateitetration);
                    dispatch($orderRequestJob)->onQueue('low');
                }
            }
        })->cron("45 12,23 * * *");
    }

    private function requestHistoricalOrderReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at','=', date('d'))->where('report_type', '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '0')->first();
                if (!empty($amzDateitetration)) {
                    $orderRequestJob = new RequestReport($marketplace, '_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_', $amzDateitetration);
                    dispatch($orderRequestJob)->onQueue('low');
                }
            }
        })->hourly();
    }

    private function requestProductReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (UserMarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestReport($marketplace, '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_');
                dispatch($productRequestJob)->onQueue('low');
            }
        })->cron("0 21 * * *");
    }

}
