<?php

namespace App\Jobs;

use App\Models\Amazon_requestlog;
use App\Models\Amzdateiteration;
use App\Models\Mws_adjustment_inventory_data;
use App\Models\Mws_afn_inventory_data;
use App\Models\Mws_excess_inventory_data;
use App\Models\Mws_inventory_details;
use App\Models\Mws_order;
use App\Models\Mws_order_returns;
use App\Models\Mws_product;
use App\Models\Mws_reserved_inventory;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Mws_warehouse_wise_inventory;
use App\Models\Usermarketplace;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
class ProcessReportsHighJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $marketplace;
    /**
     * @var \MarketplaceWebService_Client
     */
    protected $service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        $this->marketplace = $marketplace;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->service = $this->getReportsClient();

            foreach (Amazon_requestlog::getPendingReportsHigh($this->marketplace->id) as $Amazon_requestlog) {
                try {
                    $this->processReport($Amazon_requestlog);

                } catch (\Exception $ex) {
                    Log::error("Could not process ProcessReportsJob for account {$this->marketplace->id}: " . $ex->getMessage());
                    $this->log_it($ex->getMessage());
                    $this->log_it($ex->getTraceAsString());
                    throw new \Exception($ex->getMessage());
                }
            }

        } catch (\Exception $ex) {
            Log::error("ProcessReportsJob for account {$this->marketplace->id} failed: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @param Amazon_requestlog $amazon_report
     */
    private function processReport(Amazon_requestlog $amazon_report)
    {
        Log::debug("Processing amazon_report {$amazon_report->id}");

        if (!$this->canProcess($amazon_report)) {
            Log::error("Could not process report {$amazon_report->id}");
            print("this report is not processable " . $amazon_report->id);
            return;
        }
        switch ($amazon_report->report_type) {
            case Amazon_requestlog::$MERCHANT_LISTINGS:
                Mws_product::processReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;

            case Amazon_requestlog::$ORDER_MERCHANGT_LISTINGS:
                Mws_order::processReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;

            case Amazon_requestlog::$MWS_ORDER_RETURNS_LISTINGS:
                Mws_order_returns::processReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            default:
                Log::error("Can't process reports of type {$amazon_report->report_type}");
        }

        $amazon_report->save();
        $this->log_it("Amazon Report {$amazon_report->id} is processed");
        Log::debug("Amazon Report {$amazon_report->id} has been processed");
    }

    /**
     * @param Amazon_requestlog $amazon_report
     * @return mixed
     */
    private function readReport(Amazon_requestlog $amazon_report)
    {
        $request = new \MarketplaceWebService_Model_GetReportRequest();
        $this->initRequest($request, 'setMerchant', 'setMarketplace');
        $request->setReport(@fopen('php://memory', 'rw+'));
        $request->setReportId($amazon_report->report_id);
        $response = $this->service->getReport($request);

        if ($response->isSetGetReportResult()) {
            return $request->getReport();
        }

        return null;
    }


    /**
     * @param Amazon_requestlog $amazon_report
     * @param bool|true $check_status
     * @return bool
     */
    private function canProcess(Amazon_requestlog $amazon_report, $check_status = true)
    {
        if ($amazon_report->status == Amazon_requestlog::$DONE && $amazon_report->processed == 0) {
            return true;
        } else if ($check_status && in_array($amazon_report->status, [Amazon_requestlog::$IN_PROGRESS, Amazon_requestlog::$SUBMITTED])) {
            $request = new \MarketplaceWebService_Model_GetReportRequestListRequest();
            $request->getReportRequestIdList($amazon_report->request_id);
            $this->initRequest($request, 'setMerchant');
            $id_list = new \MarketplaceWebService_Model_IdList();
            $request->setReportRequestIdList($id_list->withId($amazon_report->request_id));
            $response = $this->service->getReportRequestList($request);
            $result = $response->getGetReportRequestListResult();
            $report_info = $result->getReportRequestInfoList();
            $amazon_report->status = $report_info[0]->getReportProcessingStatus();
            if ($amazon_report->status == Amazon_requestlog::$DONE) {
                $amazon_report->available_at = Carbon::now();
                $amazon_report->report_id = $report_info[0]->getGeneratedReportId();
            } elseif ($amazon_report->status == Amazon_requestlog::$NO_DATA) {
                $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
                $amazon_report->available_at = Carbon::now();
                $amazon_report->processed = 1;
                if(!empty($amzDateitetration)){
                    switch ($amazon_report->report_type) {
                        case Amazon_requestlog::$ORDER_MERCHANGT_LISTINGS:
                            $amzDateitetration->OrderStatus = '1';
                            break;
                        case Amazon_requestlog::$MWS_ORDER_RETURNS_LISTINGS:
                            $amzDateitetration->ReturnStatus = '1';
                            break;
                    }
                    $amzDateitetration->save();
                }
            } else if ($amazon_report->status == Amazon_requestlog::$CANCELLED) {
                $amazon_report->processed = 1;
            }
            $amazon_report->save();
            return $this->canProcess($amazon_report, false);
        }
        return false;
    }
}
