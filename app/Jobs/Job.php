<?php

namespace App\Jobs;

use App\Models\Mwsdeveloperaccount;
use Illuminate\Bus\Queueable;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class Job
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "onQueue" and "delay" queue helper methods.
    |
    */

    use Queueable;
    protected $mLogger;

    public function __construct()
    {
        $this->mLogger = new Logger(__CLASS__);
        $this->mLogger->pushHandler(
            new StreamHandler(storage_path('logs') . "/jobs/".get_dashed_name($this)."/{$this->marketplace->id}_jobs.log",
                Logger::WARNING));
    }

    public function log_it($msg){
        //$this->mLogger->addAlert($msg);
    }

    /**
     * @var
     */
    protected $background_id;

    /**
     * @var Logger
     */
    protected $logger;


    /**
     * @param $string
     */
    protected function info($string){
        call_user_func_array('console', func_get_args());
    }

    /**
     * @return User
     */
    protected function getAccount(){}
    protected function perform(){}


    /**
     * @param $request
     * @param bool|false $marketplace_id
     * @param string $merchantMethod
     */
    protected function initRequest($request, $merchantMethod='setSellerId', $setMarketPlace = null){
        $request->setMWSAuthToken(base64_decode($this->marketplace->mws_authtoken));
        $request->$merchantMethod(base64_decode($this->marketplace->mws_sellerid));

        if(!is_null($setMarketPlace)){
            $devaccount = Mwsdeveloperaccount::where('marketplace',  $this->marketplace->country)->first();
            $request->$setMarketPlace($devaccount->marketplace_id);
        }
    }

    public static function getMWSConfig(Mwsdeveloperaccount $devaccount, $uri){
        return [
            'ServiceURL' => "{$devaccount->marketplace_url}{$uri}",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 10,
        ];
    }

    public static function getMWSConfigInventory(Mwsdeveloperaccount $marketplace, $uri){
        return [
            'ServiceURL' => "{$marketplace->host}{$uri}",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 10,
        ];
    }

    /**
     * @param string $uri
     * @return array
     */
    private function getKeys($uri=''){
        add_to_path('Libraries');
        $dev_account = Mwsdeveloperaccount::where('marketplace',  $this->marketplace->country)->first();

        return [
            base64_decode($dev_account->access_key),
            base64_decode($dev_account->secret_key),
            self::getMWSConfig($dev_account, $uri)
        ];
    }

    private function getInventoryKeys($uri=''){
        add_to_path('Libraries');
        $dev_account = $this->account->dev_account;
        $uri = 'FulfillmentInventory/2010-10-01';
        return [
            $dev_account->access_key,
            $dev_account->secret_key,
            self::getMWSConfigInventory($this->account->marketplace, $uri)
        ];
    }

    /**
     * @return \MarketplaceWebService_Client|\MarketplaceWebService_Client
     */
    protected function getReportsClient(){
        list($access_key , $secret_key, $config) = $this->getKeys();

        return new \MarketplaceWebService_Client(
            $access_key,
            $secret_key,
            $config,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION')
        );
    }

    /**
     * @return \MWSSubscriptionsService_Client
     */
    protected function getSubscriptionClient(){
        list($access_key , $secret_key, $config) = $this->getKeys();
        return new \MWSSubscriptionsService_Client($access_key, $secret_key, env('APPLICATION_NAME'),
            env('APPLICATION_VERSION'), $config);
    }

    protected function getReportsClientFBAInventory()
    {
        list($access_key, $secret_key, $config) = $this->getInventoryKeys();
        return new  \FBAInventoryServiceMWS_Client(
            $access_key,
            $secret_key,
            $config,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION')
        );
    }
    protected function getReportsClientFBAInbound()
    {
        list($access_key, $secret_key, $config) = $this->getInboundKeys();
        return new  \FBAInboundServiceMWS_Client(
            $access_key,
            $secret_key,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION'),
            $config
        );
    }
    private function getInboundKeys(){
        add_to_path('Libraries');
        $dev_account = Mwsdeveloperaccount::where('marketplace',  $this->marketplace->country)->first();
        $uri = '/FulfillmentInboundShipment/2010-10-01';
        return [
            base64_decode($dev_account->access_key),
            base64_decode($dev_account->secret_key),
            self::getMWSConfig($dev_account, $uri)

        ];
    }
}
