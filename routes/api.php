<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['namespace' => 'Api'], function () {

    // Authentication Route
    Route::post('auth/login', 'AuthController@login');

    Route::group(['middleware' => 'jwt.auth'], function () {
        // Profile Route
        Route::post('logout', 'AuthController@logout');

        // Order return
        Route::get('order-return', 'OrderReturnController@index');

        // Product Listing
        Route::get('product-listing', 'ProductListingQualityController@index');

        // User marketplace
        Route::get('user-marketplace', 'UserMarketplaceController@index');
    });
});
